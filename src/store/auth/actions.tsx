import * as types from './types';
import * as api from '../../app/services/auth_service';

const LoginStartedAction = () => ({
  type: types.LOGIN_STARTED
})

const LoginSuccessAction = (token: string, user: any) => {
  return {
    type: types.LOGIN_SUCCESS,
    token: token,
    user: user,
  }
}

const LoginFailedAction = (error: string) => ({
  type: types.LOGIN_FAILED,
  error,
})

export const login = (data: {username: string, password: string}) => 
  (dispatch: any, state: any) => {
    dispatch(LoginStartedAction());
    api
      .login(data)
      .then(
        res => {
          if(res.status !== 200) {
            dispatch(LoginFailedAction(`${res.status}`));
          } else {
            res
              .text()
              .then(val => {
                const responseObject = JSON.parse(val);
                if(responseObject.code === 0) {
                  dispatch(LoginSuccessAction(
                    responseObject.data.token,
                    responseObject.data.user));
                } else {
                  dispatch(LoginFailedAction('Incorrect password'));
                }
              })
          }
        }
      )
  }