export const LOGIN_STARTED = 'LOGIN_STARTED';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

interface LoginStartedAction {
  type: typeof LOGIN_STARTED;
}

interface LoginSuccesAction {
  type: typeof LOGIN_SUCCESS;
  token: string;
  user: {}
}

interface LoginFailedAction {
  type: typeof LOGIN_FAILED;
  error: string;
}

export type LoginActionTypes = LoginStartedAction | LoginSuccesAction | LoginFailedAction;