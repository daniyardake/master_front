import * as types from "./types";
import Service from '../../app/services';
import * as MCService from '../../app/services/masterclasses_service';

const service = new Service();

// --------------- MASTERCLASSES ----------------

function fetchMasterclasses() {
  return {
    type: types.FETCH_MASTERCLASSES_STARTED,
  };
}

function fetchMasterclassesSuccess(masterclasses: types.Masterclass[]) {
  return {
    type: types.FETCH_MASTERCLASSES_SUCCESS,
    masterclasses,
  };
}

function fetchMasterclassesFailure(error: string) {
  return {
    type: types.FETCH_MASTERCLASSES_FAILURE,
    error,
  };
}

export const fetchAllMasterclasses = () => (dispatch: any, state: any) => {
  dispatch(fetchMasterclasses());
  MCService.getAllMasterclasses(state().auth.token)
    .then((res: any) => {
      if(res.status !== 200) {
        dispatch(fetchMasterclassesFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          if(responseObject.code === 0){
            dispatch(fetchMasterclassesSuccess(responseObject.data))
          } else{
            dispatch(fetchMasterclassesFailure(responseObject.message))
          }
        })
      }
      dispatch(fetchMasterclassesSuccess(res.data))
    })
    .catch((err: string) => dispatch(fetchMasterclassesFailure(err)));
};

// ----------------- MASTERCLASS COVER ------------------

const coverSelected = (coverUrl: string) => {
  return {
    type: types.SELECT_COVER,
    coverUrl,
  };
}

const uploadCoverStarted = () => {
  return {
    type: types.UPLOAD_COVER_STARTED,
  };
}

const uploadCoverSuccess= (image: types.Image) => {
  return {
    type: types.UPLOAD_COVER_SUCCESS,
    image,
  };
}

const uploadCoverFailure= (error: string) => {
  return {
    type: types.UPLOAD_COVER_FAILURE,
    error,
  };
}

export const selectCover = (cover: string) => (dispatch: any, state: any) => {
  dispatch(coverSelected(cover));
}

export const uploadCover = (data: {image: File}) => (dispatch: any, state: any) => {
  dispatch(uploadCoverStarted())
  MCService.uploadCover(state().auth.token, data)
    .then((res: any) => {
      if(res.status !== 201) {
        dispatch(getImageFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          if(responseObject.code === 0){
            dispatch(uploadCoverSuccess(responseObject.data));
          } else{
            dispatch(uploadCoverFailure(responseObject.message))
          }
        })
      }
      dispatch(uploadCoverSuccess(res.data));
    })
    .catch((err: string) => dispatch(uploadCoverFailure(err)));
};

// ----------------- UPLOAD VIDEO ------------------

const videosSelected = (videos: types.VIDEO[]) => {
  return {
    type: types.SELECT_VIDEOS,
    videos,
  };
}

function uploadVideoRequest() {
  return {
    type: types.UPLOAD_VIDEO_STARTED,
  };
}

function uploadVideoSuccess() {
  return {
    type: types.UPLOAD_VIDEO_SUCCESS,
  };
}

function uploadVideoFailure(error: string) {
  return {
    type: types.UPLOAD_VIDEO_FAILURE,
    error,
  };
}

export const selectVideos = (videos: types.VIDEO[]) => (dispatch: any, state: any) => {
  dispatch(videosSelected(videos));
}

export const uploadVideo = (file: File) => (dispatch: any, state: Function) => {
  dispatch(uploadVideoRequest());
  MCService.uploadVideo(state().auth.token, {file: file})
    .then((data: any) => {
      dispatch(uploadVideoSuccess());
    })
    .catch((err: string) => dispatch(uploadVideoFailure(err)));
};

// ---------------------- GET IMAGE --------------------------
function getImageStarted() {
  return {
    type: types.GET_IMAGE_STARTED,
  };
}

function getImageSuccess(url: string) {
  return {
    type: types.GET_IMAGE_SUCCESS,
    url,
  };
}

function getImageFailure(error: string) {
  return {
    type: types.GET_IMAGE_FAILURE,
    error,
  };
}

export const getImage = (photoId: string) => (dispatch: any, state: any) => {
  dispatch(getImageStarted());
  MCService.getImage(state().auth.token, photoId)
    .then((res: any) => {
      if(res.status !== 200) {
        dispatch(getImageFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          if(responseObject.code === 0){
            dispatch(getImageSuccess(responseObject.data))
          } else{
            dispatch(getImageFailure(responseObject.message))
          }
        })
      }
      dispatch(getImageSuccess(res.data))
    })
    .catch((err: string) => dispatch(getImageFailure(err)));
};

// ---------------------- CREATE MASTERCLASS (MC) --------------------------

function createMCStarted() {
  return {
    type: types.CREATE_MC_STARTED,
  };
}

function createMCSuccess(masterclass: types.Masterclass) {
  return {
    type: types.CREATE_MC_SUCCESS,
    masterclass,
  };
}

function createMCFailure(error: string) {
  return {
    type: types.CREATE_MC_FAILURE,
    error,
  };
}

export const createMC = () => (dispatch: any, state: any) => {
  dispatch(createMCStarted());
  MCService.createMC(state().auth.token)
    .then((res: any) => {
      if(res.status !== 201) {
        dispatch(createMCFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          if(responseObject.code === 0){
            dispatch(createMCSuccess(responseObject.data))
          } else{
            dispatch(createMCFailure(responseObject.message))
          }
        })
      }
      dispatch(createMCSuccess(res.data))
    })
    .catch((err: string) => dispatch(createMCFailure(err)));
};

// ---------------------- UPDATE MASTERCLASS (MC) --------------------------

function updateMCStarted() {
  return {
    type: types.UPDATE_MC_STARTED,
  };
}

function updateMCSuccess(masterclass: types.Masterclass) {
  return {
    type: types.UPDATE_MC_SUCCESS,
    masterclass,
  };
}

function updateMCFailure(error: string) {
  return {
    type: types.UPDATE_MC_FAILURE,
    error,
  };
}

export const updateMC = (data: any, id: number) => (dispatch: any, state: any) => {
  dispatch(updateMCStarted());
  MCService.updateMC(state().auth.token, data, id)
    .then((res: any) => {
      if(res.status !== 200) {
        dispatch(updateMCFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          console.log("~~~~RESPONSE~~~~~: ", responseObject)
          if(responseObject.code === 0){
            dispatch(updateMCSuccess(responseObject.data))
          } else{
            dispatch(updateMCFailure(responseObject.message))
          }
        })
      }
      dispatch(updateMCSuccess(res.data))
    })
    .catch((err: string) => dispatch(updateMCFailure(err)));
};

// ---------------------- Get Categories --------------------------

function getCategoriesStarted() {
  return {
    type: types.GET_CATEGORIES_STARTED,
  };
}

function getCategoriesSuccess(categories: types.Category[]) {
  return {
    type: types.GET_CATEGORIES_SUCCESS,
    categories,
  };
}

function getCategoriesFailure(error: string) {
  return {
    type: types.GET_CATEGORIES_FAILURE,
    error,
  };
}

export const getCategories = () => (dispatch: any, state: any) => {
  dispatch(getCategoriesStarted());
  MCService.getCategories(state().auth.token)
    .then((res: any) => {
      if(res.status !== 200) {
        dispatch(getCategoriesFailure(res.status));
      } else {
        res.
        text()
        .then((val: any) => {
          const responseObject = JSON.parse(val)
          if(responseObject.code === 0){
            dispatch(getCategoriesSuccess(responseObject.data))
          } else{
            dispatch(getCategoriesFailure(responseObject.message))
          }
        })
      }
      dispatch(getCategoriesSuccess(res.data))
    })
    .catch((err: string) => dispatch(getCategoriesFailure(err)));
};