//    ------------ MASTERCLASSES -------------

export interface Episode {
  id: number
  order: number
  title: string
  description: string
}

export interface Masterclass {
  id: number
  title: string
  description: string
  price: number
  cover_image: string
  episode_count: number
  hour_count: number
  bought_count: number
  rating: number
  episodes: Episode,
  status: string,
  author: {
    full_name: string,
    user_photo: string,
    username: string,
  },
  categories: string[]
}

export interface MasterclassesState {
  masterclasses: Masterclass[];
}

export const FETCH_MASTERCLASSES_STARTED = "FETCH_MASTERCLASSES_STARTED";
export const FETCH_MASTERCLASSES_SUCCESS = "FETCH_MASTERCLASSES_SUCCESS";
export const FETCH_MASTERCLASSES_FAILURE = "FETCH_MASTERCLASSES_FAILURE";

interface FetchMasterclassesAction {
  type: typeof FETCH_MASTERCLASSES_STARTED;
}

interface FetchMasterclassesSuccessAction {
  type: typeof FETCH_MASTERCLASSES_SUCCESS;
  masterclasses: Masterclass[]
}

interface FetchMasterclassesFailureAction {
  type: typeof FETCH_MASTERCLASSES_FAILURE;
  error: string
}

export type MasterclassesActionTypes = FetchMasterclassesAction | FetchMasterclassesSuccessAction |FetchMasterclassesFailureAction;

// ---------------- UPLOAD MASTERCLASS COVER -----------------

export interface Image {
  id: string
  image: string
}

export const SELECT_COVER = "SELECT_COVER";
export const UPLOAD_COVER_STARTED = "UPLOAD_COVER_STARTED";
export const UPLOAD_COVER_SUCCESS = "UPLOAD_COVER_SUCCESS";
export const UPLOAD_COVER_FAILURE = "UPLOAD_COVER_FAILURE";

interface SelectCoverAction {
  type: typeof SELECT_COVER;
  coverUrl: string
}

interface UploadCoverStartedAction {
  type: typeof UPLOAD_COVER_STARTED;
}

interface UploadCoverSuccessAction {
  type: typeof UPLOAD_COVER_SUCCESS;
  image: Image
}

interface UploadCoverFailureAction {
  type: typeof UPLOAD_COVER_FAILURE;
  error: string
}

export type UploadCoverActionTypes = SelectCoverAction | UploadCoverStartedAction |UploadCoverSuccessAction | UploadCoverFailureAction;

// --------------------- UPLOAD VIDEO -------------------------

export const SELECT_VIDEOS = "SELECT_VIDEOS";
export const UPLOAD_VIDEO_REQUEST = "UPLOAD_VIDEO_REQUEST";
export const UPLOAD_VIDEO_STARTED = "UPLOAD_VIDEO_STARTED";
export const UPLOAD_VIDEO_SUCCESS = "UPLOAD_VIDEO_SUCCESS";
export const UPLOAD_VIDEO_FAILURE = "UPLOAD_VIDEO_FAILURE";

export interface VIDEO {
  url: string,
  video: File,
}

interface SelectVideoAction {
  type: typeof SELECT_VIDEOS;
  videos: VIDEO[]
}

interface UploadVideoRequestAction {
  type: typeof UPLOAD_VIDEO_REQUEST;
}

interface UploadVideoStartedAction {
  type: typeof UPLOAD_VIDEO_STARTED;
}

interface UploadVideoSuccessAction {
  type: typeof UPLOAD_VIDEO_SUCCESS;
}

interface UploadVideoFailureAction {
  type: typeof UPLOAD_VIDEO_FAILURE;
  error: string;
}

export type UploadVideoActionTypes = UploadVideoRequestAction | UploadVideoStartedAction | UploadVideoSuccessAction | UploadVideoFailureAction | SelectVideoAction;


// -------------------------- get Image -----------------------------

export const GET_IMAGE_STARTED = "GET_IMAGE_STARTED";
export const GET_IMAGE_SUCCESS = "GET_IMAGE_SUCCESS";
export const GET_IMAGE_FAILURE = "GET_IMAGE_FAILURE";

interface GetImageStartedAction {
  type: typeof GET_IMAGE_STARTED;
}

interface GetImageSuccessAction {
  type: typeof GET_IMAGE_SUCCESS
  url: string
}

interface GetImageFailureAction {
  type: typeof GET_IMAGE_FAILURE;
  error: string;
}

export type GetImageActionTypes = GetImageStartedAction | GetImageSuccessAction | GetImageFailureAction;

// -------------------------- Create Masterclass -----------------------------

export const CREATE_MC_STARTED = "CREATE_MC_STARTED";
export const CREATE_MC_SUCCESS = "CREATE_MC_SUCCESS";
export const CREATE_MC_FAILURE = "CREATE_MC_FAILURE";

interface CreateMCStartedAction {
  type: typeof CREATE_MC_STARTED;
}

interface CreateMCSuccessAction {
  type: typeof CREATE_MC_SUCCESS
  masterclass: Masterclass
}

interface CreateMCFailureAction {
  type: typeof CREATE_MC_FAILURE
  error: string
}

export type CreateMCActionTypes = CreateMCStartedAction | CreateMCSuccessAction | CreateMCFailureAction;

// -------------------------- Update Masterclass -----------------------------

export const UPDATE_MC_STARTED = "UPDATE_MC_STARTED";
export const UPDATE_MC_SUCCESS = "UPDATE_MC_SUCCESS";
export const UPDATE_MC_FAILURE = "UPDATE_MC_FAILURE";

interface UpdateMCStartedAction {
  type: typeof UPDATE_MC_STARTED;
}

interface UpdateMCSuccessAction {
  type: typeof UPDATE_MC_SUCCESS
  masterclass: Masterclass
}

interface UpdateMCFailureAction {
  type: typeof UPDATE_MC_FAILURE
  error: string
}

export type UpdateMCActionTypes = UpdateMCStartedAction | UpdateMCSuccessAction | UpdateMCFailureAction;

// -------------------------- Get Categories -----------------------------

export interface Category {
  id: number
  order: number
  name: string
}

export const GET_CATEGORIES_STARTED = "GET_CATEGORIES_STARTED";
export const GET_CATEGORIES_SUCCESS = "GET_CATEGORIES_SUCCESS";
export const GET_CATEGORIES_FAILURE = "GET_CATEGORIES_FAILURE";

interface GetCategoriesStartedAction {
  type: typeof GET_CATEGORIES_STARTED;
}

interface GetCategoriesSuccessAction {
  type: typeof GET_CATEGORIES_SUCCESS
  categories: Category[]
}

interface GetCategoriesFailureAction {
  type: typeof GET_CATEGORIES_FAILURE
  error: string
}

export type GetCategoriesActionTypes = GetCategoriesStartedAction | GetCategoriesSuccessAction | GetCategoriesFailureAction;
