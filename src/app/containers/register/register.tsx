import React, { Component } from 'react';
import { connect} from 'react-redux'
import {Link} from 'react-router-dom';

import {login} from '../../../store/auth/actions';
import Search from '../../components/search';
import './register.scss';

interface Props {
  login: typeof login
  isLoggingIn: boolean
  isLoggedIn: boolean
}

interface State {
  username: string
  password: string
}

class Register extends Component<Props, State> {
    state = {
      username: '',
      password: ''
    }

    setUsername = (val: string) => {
      this.setState(() => ({username: val}))
    }

    setPassword = (val: string) => {
      this.setState(() => ({password: val}))
    }

    loginClicked = () => {
      const {username, password} = this.state;
      this.props.login({username: username, password: password});
      this.setState(() => ({username: '', password: ''}))
    }

    render(){
      const {username, password} = this.state
        return (
            <div className='login-page ms-flex-column ms-justify-center ms-align-center'>
              {!this.props.isLoggingIn && <div className="ms-flex-column  ms-justify-center ms-align-center">
                <div className="title">Registration</div>
                <Search value={username} handleChange={this.setUsername}
                          className='input' placeholder='Full name'/>
                <Search value={password} handleChange={this.setPassword}
                          className='input' placeholder='Username' type="text"/>
                <Search value={password} handleChange={this.setPassword}
                          className='input' placeholder='Email' type="text"/>
                <Search value={password} handleChange={this.setPassword}
                          className='input' placeholder='Password' type="password"/>
                
                <div className="ms-flex-inline ms-justify-center ms-align-center">
                  <div onClick={this.loginClicked} className="btn">Sign up</div>
                  <div className="btn-link">or</div>
                  <Link to='login' className="btn-link">Sign in</Link>
                </div>
              </div>}
              {this.props.isLoggingIn && 
                <div className="btn-link">Loading...</div>}
            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    isLoggingIn: state.auth.isLoggingIn,
    isLoggedIn: state.auth.isLoggedIn
});

export default connect(
    mapStateToProps,
    { login }
)(Register);
