import React, { Component } from 'react';
import { connect} from 'react-redux'
import {History} from 'history'
import { withRouter, RouteComponentProps } from "react-router-dom";

import Cover from '../../images/default-cover.jpeg';
import VideoCard from '../../components/video_card';
import DefaultCover from '../../images/default-cover.jpeg';

import './masterclass.scss';

interface Props extends RouteComponentProps {
}

interface State {
}

const MC = {
  tags: ['yoga', 'fitness'],
  author: 'Nurdaulet Akhanov',
  title: 'Chakra Flow',
  price: 'Price: 2000 kzt',
  videos: [{
    title: 'Video',
    description: 'lorem ipsum dolor sit amet',
    id: 1
  },
  {
    title: 'Video',
    description: 'lorem ipsum dolor sit amet',
    id: 2
  }],
  avatar: '',
  cover: '',
  numberOfLessons: 14,
  totalHours: 72,
  description: 'Chakra-flow is an online course consisting of 7 creative flow in a stream and 7 amazing meditations-trips with emphasis on each chakra!'
}

class Masterclass extends Component<Props, State> {
    state = {
    }

    render(){
        const {title, description, price} = MC;
        return (
            <div className="masterclass-wrapper">
              <SubHeader history={this.props.history}/>
              <img src={DefaultCover} className="cover" alt=""/>
              <div className="body">
                <div className="title">{title}</div>
                <div className="description">{description}</div>
                <div className="ms-flex-inline ms-justify-end">
                  <div className="description">{price}</div>
                </div>
                <div className="lessons">
                  {
                    MC.videos.map((v, ix) => (
                      <VideoCard key={ix}/>
                    ))
                  }
                </div>
              </div>
            </div>
        );
    }
}

interface headerProps {
  history: History
}

function SubHeader(props: headerProps) {
  return <div className="back-header">
      <div className="back-button ms-flex-inline" onClick={props.history.goBack}>
          Back
      </div>
  </div>
}

const mapStateToProps = (state: any) => ({
});

export default withRouter(connect(
    mapStateToProps,
    { }
)(Masterclass));