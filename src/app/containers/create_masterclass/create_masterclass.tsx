import React, { Component } from 'react';
import {History} from 'history';
import { connect} from 'react-redux'
import { withRouter, RouteComponentProps } from "react-router-dom";
import Dropzone from 'react-dropzone'

import Search from '../../components/search';
import TextArea from '../../components/text_area';
import  MultipleSelect from '../../components/multiple_select';
import VideoCard from '../../components/video_card';
import {selectCover, uploadVideo, uploadCover, selectVideos, createMC, updateMC, getCategories} from '../../../store/masterclasses/actions';
import PlusIcon from '../../images/plus.png';
import * as types from '../../../store/masterclasses/types';

import './create_masterclass.scss';

interface State {
    uploaded: boolean
    title: string
    categories: types.Category[]
    price: string
    description: string
    ownUpdate: boolean
}

interface Props extends RouteComponentProps {
    selectCover: typeof selectCover
    cover: types.Image
    uploadVideo: typeof uploadVideo
    videoIsUploading: boolean
    uploadCover: typeof uploadCover
    coverIsUploading: boolean
    selectVideos: typeof selectVideos
    videos: types.VIDEO[]
    createMC: typeof createMC
    draftMC: types.Masterclass
    updateMC: typeof updateMC
    updatedMC: types.Masterclass
    getCategories: typeof getCategories
    categories: types.Category[]
}

class CreateMasterclass extends Component<Props, State> {
    coverRef = React.createRef<HTMLDivElement>();
    initialCategories: types.Category[] = []

    state = {
        uploaded: false,
        title: '',
        categories: this.initialCategories,
        description: '',
        price: '',
        ownUpdate: false,
        cover_image: this.props.cover,
    }

    componentDidMount() {
        if(this.props.cover) {
            this.setState(() => ({uploaded: true}));
            this.coverRef.current.style.backgroundImage = `url(${this.props.cover})`;
        }
        if (localStorage.getItem('Draft MC') !== 'created') {
            this.props.createMC()
            this.props.getCategories()
        }
    }

    componentWillUnmount() {
        localStorage.removeItem('Draft MC')
    }

    handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const cover = event.currentTarget.files[0];
        const url = window.URL.createObjectURL(new Blob([cover], {type: "file"}));
        this.props.selectCover(url);
        this.props.uploadCover({image: cover});
        this.coverRef.current.style.backgroundImage = `url(${url})`;

        this.setState(() => ({
            uploaded: true,
        }));
    }

    onDrop = (acceptedFiles: File[]) => {
        
        let selectedVideos: types.VIDEO[] = [];
        acceptedFiles.map(file => {
            const url = window.URL.createObjectURL(new Blob([file], {type: "file"}));
            selectedVideos.push({url, video: file});
            // this.props.uploadVideo(file);
        })
        this.props.selectVideos(selectedVideos);
    }

    tagSelected = (category: types.Category) : boolean => {
        const {categories} = this.state;
        if(categories.find((el) => (category === el))) {
            let newCategories = [...categories];
            const ind = newCategories.findIndex((el) => (el === category));
            newCategories.splice(ind, 1);
            this.setState(() => ({categories: newCategories}));
            return false;
        }

        if(categories.length < 3) {
            let newCategories = [...categories];
            newCategories.push(category);
            this.setState(() => ({
                categories: newCategories
            }));

            return true;
        }

        return false;
    }

    handleTitleChange = (value: string) => {
        this.setState(() => ({
            title: value, 
        }));
    }

    handlePriceChange = (value: string) => {
        this.setState(() => ({
            price: value, 
        }));
    }

    handleDescriptionChange = (val: any) => {
        console.log('!!!: ', val);
    }

    handleUpdateMC = () => {
        const categories = this.state.categories.map(cat => (cat.id))
        const data = {
            title: this.state.title,
            description: this.state.description,
            price: this.state.price,
            categories: categories,
            status: 'draft',
            cover_image: this.props.cover.id,
        }
        this.props.updateMC(data, this.props.draftMC.id)
    }
    render(){
        const {uploaded = ''} = this.state;
        return (
            <div className="create-mc">
                <SubHeader history={this.props.history}/>
                <div ref={this.coverRef} className="upload-cover ms-flex-inline ms-align-center ms-justify-center">
                    <div>
                        <label htmlFor="cover"
                            className="cover-label ms-flex-column ms-align-center ms-justify-center">
                            <img src={PlusIcon} alt=""/>
                            <div>{uploaded ? "change cover" : "Tap here to add a cover"}</div>
                        </label>
                        <input id="cover" type="file" className="upload-input"
                            onChange={this.handleChange} />
                    </div>
                </div>
                <div className="constructor ms-flex-column">
                    <div className="forms ms-flex-inline ms-space-between">
                        <div className="ms-flex-column">
                            <Search className="ms-input" value={this.state.title} handleChange={this.handleTitleChange} placeholder="Masterclass title"/>
                            <div className="select-categories">
                                <MultipleSelect 
                                    className="tag-selector"
                                    selected={this.state.categories}
                                    tags={this.props.categories}
                                    onSelect={this.tagSelected}/>
                            </div>
                            <Search className="ms-input price-input" value={this.state.price} handleChange={this.handlePriceChange} placeholder="Price"/>
                        </div>
                        <TextArea className="ms-textarea" 
                            value={this.state.description}
                            columns={20} rows={10}
                            placeholder="Description..."
                            handleChange={this.handleDescriptionChange}
                        ></TextArea>
                    </div>
                    <div className="videos ms-flex-column">
                        <div className="select-video">
                            <Dropzone onDrop={this.onDrop}>
                                {({getRootProps, getInputProps, isDragActive}) => (
                                    <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    {isDragActive ? "Drop files here" : 'Click or drag a video file to upload'}
                                    </div>
                                )}
                            </Dropzone>
                        </div>
                        <div className="ms-flex ms-flex-column">
                            <div>
                                {
                                    this.props.videos.map((v, k) => (
                                        <VideoCard cover={v.url}/>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ms-btn-long" onClick={this.handleUpdateMC}>Create masterclass</div>
            </div>
        );
    }
}

interface headerProps {
    history: History
}

function SubHeader(props: headerProps) {
    return <div className="back-header">
        <div className="back-button ms-flex-inline" onClick={props.history.goBack}>
            Cancel
        </div>
    </div>
}

const mapStateToProps = (state: any) => ({
    cover: state.masterclasses.uploadCover,
    videos: state.masterclasses.selectVideos,
    videoIsUploading: state.masterclasses.videoIsUploading,
    coverIsUploading: state.masterclasses.coverIsUploading,
    draftMC: state.masterclasses.createMC,
    updatedMC: state.masterclasses.updateMC,
    categories: state.masterclasses.categories,
});

export default withRouter(connect(
    mapStateToProps,
    {
        selectCover, 
        uploadVideo, 
        uploadCover,
        selectVideos,
        createMC,
        updateMC,
        getCategories,
    }
)(CreateMasterclass));
