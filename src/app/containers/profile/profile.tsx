import React, { Component, createRef} from 'react';
import './profile.scss';
import Header from '../../components/header'
import MasterCard from '../../components/master_card'
import Avatar from '../../components/avatar';
import { Link, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { connect} from 'react-redux'

import { fetchAllMasterclasses } from "../../../store/masterclasses/actions";
import * as types from '../../../store/masterclasses/types';

interface State {
    avatarStyle: {}
    usernameStyle: {}
    pathStyle: {}
    fullName: string
    avatar: string
    cover: string
    description: string
    username: string
}

interface Props extends RouteComponentProps {
    fetchAllMasterclasses: typeof fetchAllMasterclasses
    masterclasses: types.Masterclass[]
    isLoggedIn: boolean
}

const avatarDefaultStyle = {
    position: 'relative',
    width: '80px',
    height: '80px',
};

const avatarSecondaryStyle = {
    position: 'unset',
    width: '60px',
    height: '60px',
    marginTop: '10px'
};

const USERNAME_DEFAULT_STYLE = {
    fontSize: '24px',
};

const USERNAME_SECONDARY_STYLE = {
    fontSize: '18px',
    marginTop: '10px'
};

const PATH_DEFAULT_STYLE = {
    paddingTop: '20px'
}

const PATH_SECONDARY_STYLE = {
    paddingTop: '2px'
}

class Profile extends Component<Props, State>{
    state = {
        avatarStyle: avatarDefaultStyle,
        usernameStyle: USERNAME_DEFAULT_STYLE,
        pathStyle: PATH_DEFAULT_STYLE,
        fullName: '',
        avatar: '',
        cover: '',
        description: '',
        username: '',

    };

    componentWillMount = () => {
        this.props.fetchAllMasterclasses();
        const user = JSON.parse(localStorage.getItem('state')).auth.user;
        this.setState(() => ({
            fullName: user.full_name,
            description: user.description,
            avatar: user.user_photo,
            cover: user.cover_photo,
            username: user.username
        }))
    }

    componentDidMount = () => {
        window.addEventListener('scroll', this.listenScrollEvent);
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.listenScrollEvent);
    }

    listenScrollEvent = () => {
        if(window.scrollY > 20) {
            this.setState(() => ({
                avatarStyle: avatarSecondaryStyle,
                usernameStyle: USERNAME_SECONDARY_STYLE,
                pathStyle: PATH_SECONDARY_STYLE
            }));
        } else {
            this.setState(() => ({
                avatarStyle: avatarDefaultStyle,
                usernameStyle: USERNAME_DEFAULT_STYLE,
                pathStyle: PATH_DEFAULT_STYLE
            }));
        }
    }

    render(){
        const coverStyle = {
            backgroundImage: `url(${this.state.cover})`,
            backgroundColor: '#020202'
        };

        const {masterclasses: MS} = this.props;

        return (
            <div className="profile-page-component">
                <Header className="profile-header"/>
                <img src={this.state.cover} alt=""/>
                <div style={coverStyle} className="cover"></div>
                <div className="profile-page-wrapper">
                    <div className="user-data ms-flex-inline ms-space-between ms-flex-wrap">
                        <div className="ms-flex-inline">
                            <Avatar style={this.state.avatarStyle}
                            avatar={this.state.avatar}
                            username={this.state.username}
                            className="profile-avatar"/>
                            <div className="user-info">
                                <div className="username" style={this.state.usernameStyle}>
                                    {this.state.fullName}
                                </div>
                                <div className="page-path" style={this.state.pathStyle}>
                                    {this.state.description}
                                </div>
                            </div>
                        </div>
                        <Link className="ms-btn-long create-mc-btn ms-flex-inline ms-align-center 
                            ms-justify-center" to="/new-masterclass">Create masterclass</Link>
                    </div>
                    <div className="masterclasses">
                        {MS.map((mc, ind) => (
                            <MasterCard masterClass={mc} key={ind}/>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        masterclasses: state.masterclasses.fetchMasterclasses
    }
};

export default withRouter(connect(
    mapStateToProps,
    { fetchAllMasterclasses }
)(Profile));
