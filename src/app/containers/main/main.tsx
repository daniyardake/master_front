import React, { Component } from 'react';
import Header from '../../components/header';
import Tag from '../../components/tag'; 
import MasterCard from '../../components/master_card';
import './main.scss';

const tags: string[] = ['All', 'Sport', 'Yoga', 'Baking', 'Dancing', 'Programming', 'Math', 'Culture'];

type State = {
    currentTag: string;
}

class Main extends Component<{}, State>{

    state = {
        currentTag: "All"
    };

    onClick = (tag: string) => {
        this.setState(() => {
            return {currentTag: tag}
        })
    }


    render(){
        const{currentTag} = this.state;
        return (
            <div className="main-page-wrapper">
                <Header tag={currentTag} searchVisible={true}/>
                <div className="main-page-body">
                    <div className="tags ms-flex-inline">
                        {tags.map((tag, ind) => (
                            <Tag tag={tag} 
                                clicked={this.onClick} 
                                selected={tag===currentTag}
                                key={ind}/>
                        ))}
                    </div>
                    <div>
                        {/* {MS.map((mc, ind) => (
                            <MasterCard masterClass={mc} key={ind}/>
                        ))
                        } */}
                    </div>
                </div>
            </div>
        );
    }
}

export default Main;
