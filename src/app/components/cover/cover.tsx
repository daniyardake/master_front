import React, { Component } from 'react';
import './cover.scss';
import '../../app.scss';
import DefaultCover from '../../images/default-cover.jpeg';

class Cover extends Component{
    render(){
        return (
            <div>
                <img src={DefaultCover} className="cover"/>
            </div>
        );
    }
}

export default Cover;
