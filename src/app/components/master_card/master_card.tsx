import React, { Component } from 'react';

import './master_card.scss';
import * as types from '../../../store/masterclasses/types';
import Avatar from '../avatar';
import DefaultCover from '../../images/default-cover.jpeg';
import Tag from '../../components/tag';
import { Redirect, Link, withRouter, RouteComponentProps } from 'react-router-dom';

interface Props extends RouteComponentProps{
    masterClass: types.Masterclass
}

class MasterCard extends Component<Props>{
    handleClick = () => {
        this.props.history.push(`/masterclass/${this.props.masterClass.id}`);
    }
    render(){
        const {title, description, id, price, cover_image: cover, episode_count, hour_count, bought_count, rating, episodes, status} = this.props.masterClass;

        return (
            <div onClick={this.handleClick} className="master-card ms-flex-inline ms-align-center">
                <img src={cover} className="master-class-cover"/>
                <div className="mc-info">
                    <div className="mc-title">{title}</div>
                    <div className="ms-flex-inline ms-align-center">
                        <div className="numbers">{episode_count} lessons</div>
                        <div className="dot">·</div>
                        <div className="numbers">{hour_count} hours</div>
                    </div>
                    <div className="description">{description}</div>
                    <div className="ms-flex-inline ms-align-center">
                        {/* <Avatar path={avatar} width={20}/> */}
                        <div className="author">Author</div>
                    </div>
                    {/* <div className="tags ms-flex-inline">
                        {
                            tags.map((tag, ind) => (<Tag className="tag-chip" tag={tag} key={ind} selected={true}/>))
                        }
                    </div> */}
                </div>
            </div>
        );
    }
}

export default withRouter(MasterCard);
