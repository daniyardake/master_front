import React, { Component, ChangeEvent } from 'react';
import './text_area.scss';

interface Props {
    value: string
    handleChange: Function
    className?: string
    placeholder?: string
    columns?: number
    rows?: number
}

interface State {
    value: string
    ownUpdate: boolean
}

class TextArea extends Component<Props, State> {

    constructor(props: Props){
        super(props);
        this.state = {value: '', ownUpdate: false}
    }

    static getDerivedStateFromProps = (nextProps: Props, prevState: State) => {
        if(nextProps.value !== prevState.value){
            return {value: nextProps.value, ownUpdate: false};
        }
        return null;
    }

    changed = (event: ChangeEvent<HTMLTextAreaElement>) => {
      console.log(event.currentTarget.value)
      this.props.handleChange(event.currentTarget.value);
    }

    render(){
        const {value} = this.state;
        const {placeholder, className, columns, rows} = this.props;
        const classNames = 'search-bar ' + className;

        return(
          <textarea className={className} 
              cols={columns} rows={rows}
              placeholder={placeholder}
              onChange={this.changed}
          ></textarea>
        );
    }
}

export default TextArea;
