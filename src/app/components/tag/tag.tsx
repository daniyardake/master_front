import React, { Component } from 'react';
import './tag.scss';

type Props = {
    tag: string;
    clicked?: Function;
    selected?: boolean;
    className?: string;
};

class Tag extends Component<Props>{

    state = {
        clicked: false
    }

    clicked = () =>{
        this.props.clicked(this.props.tag);
    }

    render(){
        const {className = "default"} = this.props;

        let classNames = "chip " + className;
        if(this.props.selected){
            classNames += " selected";
        }

        return (
            <div className={classNames}
                onClick={this.clicked}
            >{this.props.tag}</div>
        );
    }
}

export default Tag;
