import React, { Component, ChangeEvent } from 'react';
import './search.scss';
import SearchLogo from '../../images/search.png';

type Props = {
    value: string
    handleChange: Function
    className?: string
    placeholder?: string
    type?: string
};

type State = {
    value: string
    ownUpdate: boolean
};

class Search extends Component<Props, State> {

    constructor(props: Props){
        super(props);
        this.state = {value: '', ownUpdate: false}
    }

    static getDerivedStateFromProps = (nextProps: Props, prevState: State) => {
        if(nextProps.value !== prevState.value){
            return {value: nextProps.value, ownUpdate: false};
        }
        return null;
    }

    changed = (event: ChangeEvent<HTMLInputElement>) => {
        this.props.handleChange(event.currentTarget.value);
    }

    render(){
        const {value} = this.state;
        const {placeholder, className, type = 'text'} = this.props;
        const classNames = 'search-bar ' + className;

        return(
            <input type={type} placeholder={placeholder} className={classNames} 
                onChange={this.changed} value={value}/>
        );
    }
}

export default Search;
