import React, { Component} from 'react';
import './multiple_select.scss';
import Tag from '../tag';
import Search from '../search';
import * as types from '../../../store/masterclasses/types';

interface State {
    opened: boolean
    filterValue: string
}

interface Props {
    tags: types.Category[]
    className?: string
    onSelect: Function
    selected: types.Category[]
}

const selectedCategires: types.Category[] = []

class MultipleSelect extends Component<Props, State> {
    container = React.createRef<HTMLDivElement>();

    state = {
        opened: false,
        filterValue: ''
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }
    componentWillUnmount() {
      document.removeEventListener("mousedown", this.handleClickOutside);
    }

    handleClickOutside = (event: any) => {
        if (this.container.current && 
            !this.container.current.contains(event.target)) {
          this.setState({
            opened: false,
          });
        }

        return;
      };

    handleClick = () => {
        this.setState((state) => ({opened: !state.opened}));
    }

    addCategory = (category: types.Category) : boolean => {
        return this.props.onSelect(category)
    }

    handleChange = (value: string) => {
        this.setState(() => ({filterValue: value}));
    }

    render(){
        const {tags, className, selected} = this.props;
        const {opened, filterValue} = this.state;
        const classNames = "multiple-select-wrapper " + className;
        return(
            <div className={classNames} ref={this.container}>
                <div className="ms-flex-inline ms-flex-wrap selector-trigger"
                    onClick={this.handleClick}>
                    {selected.map((tag, ind) => {
                        return <Tag tag={tag.name} key={ind}/>
                    })}
                    <Search value={filterValue} handleChange={this.handleChange}
                        className='filter-input' placeholder='Choose categories'/>
                </div>
                {opened && <div className="selector-popup">
                    {tags.map((tag, ind) => (
                        <Category category={tag} key={ind}
                            categorySelected={this.addCategory}
                            selected={selected.includes(tag)}/>
                    ))}
                </div>}
            </div>
        );
    }
}

type CategoryProps = {
    selected?: boolean,
    category: types.Category,
    categorySelected: Function,
}

type CategoryState = {
    selected: boolean
}

class Category extends Component<CategoryProps, CategoryState> {
    state = {
        selected: false
    }

    handleClick = () => {
        if(this.props.categorySelected(this.props.category)) {
            this.setState(() => ({selected: true}));
        } else {
            this.setState(() => ({selected: false}));
        }
    }
    render() {
        const {selected} = this.state;
        return(
            <div className={`selector-tag ms-flex-inline ms-space-between ${selected || this.props.selected ? 'selected' : ''}`}
                onClick={this.handleClick}>
                <div>{this.props.category.name}</div>
                {selected && <div>v</div>}
            </div>
        );
    }
}

export default MultipleSelect;
