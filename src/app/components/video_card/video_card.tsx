import React, { Component } from 'react';

import DefaultCover from '../../images/default-cover.jpeg';

import './video_card.scss';

const videoData = {
    title: 'Video',
    description: 'Lorem ipsum dolor sit amet, lorem ipsum dolor sit amet',
}

interface Props {
    video?: string
    cover?: string
    title?: string
    description?: string
    type?: boolean
}

class VideoCard extends Component<Props>{
    render(){
        const {title, description} = videoData;
        const {cover, video, type} = this.props;
        return (
            <div className="video-card ms-flex-inline ms-align-center">
                <img src={cover ? cover : DefaultCover} className="video-cover"/>
                <div className="mc-info">
                    <div className="mc-title video-title">{title}</div>
                    <div className="video-description">{description}</div>
                </div>
            </div>
        );
    }
}

export default VideoCard;
