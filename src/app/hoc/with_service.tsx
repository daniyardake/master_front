import React from 'react';
import { ServiceConsumer } from '../components/service_context';

const withService = () => (Wrapped: any) => {
  return (props: any) => {
    return (
      <ServiceConsumer>
        {
          (service) => {
            return (<Wrapped {...props}
                     service={service}/>);
          }
        }
      </ServiceConsumer>
    );
  }
};

export default withService;