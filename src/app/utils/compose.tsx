const compose = (...funcs: any) => (comp: any) => {
  return funcs.reduceRight(
    (wrapped: any, f: Function) => f(wrapped), comp);
};

export default compose;