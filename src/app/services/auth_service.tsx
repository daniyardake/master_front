import {API_BASE, STD_HEADERS} from './constants';
import qs from 'qs'

const loginUrl = API_BASE + "auth/login/";

const postReq = async (type: string, url: string, data: any) => (
  await fetch(
    url,
    type === 'POST' ?
    {
      method: 'POST',
      headers: STD_HEADERS,
      body: qs.stringify(data)
    } : {
      method: 'GET',
      headers: {
        ...STD_HEADERS,
        "auth-token": data,
      }
    }
  ));

export const login = (data: {username: string, password: string}) => (
  postReq('POST', loginUrl, data)
)